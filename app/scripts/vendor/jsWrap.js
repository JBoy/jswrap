'use strict';

/* jsWrap */
var jsWrap = {

    /**
     * Checks if a class exists on given element
     * @param el
     * @param className
     */
    hasClass: function (el, className) {
        if (el.classList){
            return el.classList.contains(className);
        }
        else{
            return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
        }
    },


    /**
     * Finds an element
     * @param classOrId
     */
    select: function (classOrId){
        return document.querySelectorAll(classOrId);
    },


    /**
     * Finds a childelement
     * @param el
     * @param className
     */
    find: function (el, selector){
        if(el.querySelectorAll(selector).length){
            return el.querySelectorAll(selector);
        }
        return false;
    },


    /**
     * Adds a class to given element
     * @param el
     * @param className
     */
    addClass: function (el, className) {
        console.log(el);
        if (el.classList){
            el.classList.add(className);
        }
        else{
            el.className += ' ' + className;
        }
        return el;
    },


    /**
     * Checks if a css property is supported in current browser.
     * @param property
     */
    isCssPropertySupported: function (property){
        return property in document.body.style;
    },


    /**
     * Removes a class from given element
     * @param el
     * @param className
     */
    removeClass: function (el, className) {
        if (el.classList){
            el.classList.remove(className);
        }
        else{
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
        return el;
    },


    /**
     * Toggles a class on a given element
     * @param el
     * @param className
     */
    toggleClass: function (el, className) {
        if (el.classList) {
            el.classList.toggle(className);
        } else {
            var classes = el.className.split(' ');
            var existingIndex = classes.indexOf(className);

            if (existingIndex >= 0){
                classes.splice(existingIndex, 1);
            }
            else{
                classes.push(className);
            }
            el.className = classes.join(' ');
        }
        return el;
    },


    /**
     * Gets data attribute on a given element
     * @param el
     * @param data
     */
    data: function (el, data, setvalue) {
        if (typeof setvalue === 'undefined') {
            return el.getAttribute('data-'+data);
        }
        else{
            return el.setAttribute('data-'+data, setvalue);
        }
    },

    /**
     * Append child to element
     * @param el
     * @param appendHtml
     * @returns el
     */
    append: function(el, appendHtml){
        el.innerHTML += appendHtml;
        return el;
    },

    /**
     * Prepend child to element
     * @param el
     * @param prependHtml
     * @returns el
     */
    prepend: function(el, prependHtml){
        el.innerHTML = prependHtml + el.innerHTML;
        return el;
    },


    /**
     * Empties an element
     * @param el
     */
    empty: function (el){
        el.innerHTML = '';
        return el;
    },


    /**
     * Lists all method of this object
     * @param obj
     */
    getKeys: function(obj){
        var keys = [];
        for(var key in obj){
            if(key !== 'getKeys'){
                keys.push(key);
            }
        }
        keys.push(keys.length + ' methods');
        return keys;
    }

};