'use strict';

window.onload = function(){

    var btn = jsWrap.select('button')[0],
        ul  = jsWrap.select('ul');

    btn.addEventListener('click', function(){
        jsWrap.prepend(ul[0], '<li>Another list item</li>');
    });
};
